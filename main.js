(() => {

	'use strict';

	MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

	HTMLCollection.prototype.toArray = function() {
		return Array.prototype.slice.call(this);
	};

	function parseTable(table) {

		const semester = { ues: [] };
		let ueIndex = -1, moduleIndex = -1;
		let columns;

		for (const row of table.getElementsByTagName('tr')) {
			
			columns = row.getElementsByTagName('td').toArray();

			switch (row.className) {

				case 'ue':
					semester.ues.push({
						name: columns[0].textContent,
						coeff: parseFloat(columns[4].textContent, 10),
						cell: columns[3],
						modules: []
					});
					ueIndex++;
					moduleIndex = -1;
					break;

				case 'module':
					semester.ues[ueIndex].modules.push({
						name: columns[1].textContent,
						coeff: parseFloat(columns[4].textContent, 10),
						cell: columns[3],
						marks: []
					});
					++moduleIndex;
					break;
				
				case 'eval':
					if (isNaN(columns[3].textContent)) break;
					semester.ues[ueIndex].modules[moduleIndex].marks.push({
						name: columns[2].textContent,
						coeff: parseFloat(columns[4].textContent, 10),
						value: parseFloat(columns[3].textContent, 10),
						cell: columns[3]
					});
					break;
			}
		}

		return semester;
	}

	function computeAverages(semester) {

		function f(array, valueFunc, coeffFunc) {

			let value, coeff;
			let valueSum = 0, coeffSum = 0;

			for (const elem of array) {

				value = valueFunc(elem);
				if (value === undefined) continue;
				coeff = coeffFunc(elem);

				valueSum += value * coeff;
				coeffSum += coeff;
			}

			return coeffSum === 0 ? undefined : valueSum / coeffSum;
		}

		for (const ue of semester.ues) {
			for (const module of ue.modules) {
				module.average = f(module.marks, mark => mark.value, mark => mark.coeff);
			}
			ue.average = f(ue.modules, module => module.average, module => module.coeff);
		}
		semester.average = f(semester.ues, ue => ue.average, ue => ue.coeff);
	}

	function computeBonusPoints(semester) {

		let bonus = 0;

		for (const ue of semester.ues){

			if (ue.coeff !== 0 || ue.modules.length === 0) continue;

			let max = 10;

			for (const module of ue.modules) {
				max = Math.max(max, module.average);
			}

			ue.bonus = (max - 10) * 0.005 * semester.average;
			ue.average = undefined;
			ue.coeff = undefined;

			bonus += ue.bonus;
		}

		semester.average += bonus;
	}

	function displayResult(table, semester) {

		function round(mark, isBonus) {

			if (mark === undefined) return undefined;

			const precision = isBonus ? 1000 : 100;
			mark = Math.round(mark * precision);

			let decimals = mark % precision + '';
			while (decimals.length < 2) decimals += '0';
			mark = parseInt(mark / precision, 10) + '';
			if (!isBonus) while (mark.length < 2) mark = '0' + mark;
			
			return mark + '.' + decimals;
		}

		for (const ue of semester.ues) {
			for (const module of ue.modules) {
				module.cell.textContent = round(module.average, false);
			}

			if (ue.bonus == undefined) {
				ue.cell.textContent = round(ue.average, false);
			} else {
				ue.cell.textContent = round(ue.bonus, true);
				ue.cell.nextSibling.textContent = 'bonus';
			}
		}

		const overallAverage = document.createElement('h2');
		overallAverage.textContent = 'moyenne générale : ' + round(semester.average, false);
		table.parentNode.insertBefore(overallAverage, table);
	}

	function addInfoMsg(table) {
		const msg = document.createElement('a');
		msg.textContent = 'la moyenne du bulletin peut être différente de celle du relevé de notes';
		msg.style.textDecoration = 'underline';
		table.parentNode.insertBefore(msg, table);
		msg.addEventListener('click', () => {
			alert('La moyenne qui est calculée sur votre bulletin (que vous recevez par mail) ne prend pas en compte les coefficients des UE, seulement ceux des modules. Elle n\'est donc pas forcément représentative de votre vraie moyenne.\n\nLa moyenne qui est affichée ici sera identique à celle de votre bulletin, à la fin du semestre, quand vous aurez des notes dans chaque module et donc que la somme des coefficients des modules d\'un UE sera égale au coefficient de l\'UE.');
		});
	}

	function refreshAll(ctx) {
		const table = ctx.getElementsByTagName('table')[0];
		const semester = parseTable(table);
		computeAverages(semester);
		computeBonusPoints(semester);
		displayResult(table, semester);
		addInfoMsg(table);
	}

	refreshAll(document);

	new MutationObserver(records => refreshAll(records[0].addedNodes[0])).observe(document.getElementById('dom2'), {
		subtree: true,
		attributes: true,
		childList: true
	});

})();
